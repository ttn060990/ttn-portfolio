export const UPDATE_ACCOUNT_INFO = "https://rynadb.herokuapp.com/api/updateaccountinfo";
export const CHANGE_ACCOUNT_AVATAR = "https://rynadb.herokuapp.com/api/changeaccountavatar";
export const CHANGE_ACCOUNT_PASSWORD = "https://rynadb.herokuapp.com/api/changeaccountpassword";
export const SELECT_ACCOUNT_INFO = "https://rynadb.herokuapp.com/api/selectaccountinfo";

export const ADD_NEW_PROJECT = "https://rynadb.herokuapp.com/api/addnewproject";
export const UPDATE_PROJECT_INFO = "https://rynadb.herokuapp.com/api/updateprojectinfo";
export const CHANGE_PROJECT_THUMBNAIL = "https://rynadb.herokuapp.com/api/changeprojectthumbnail";
export const CHANGE_PROJECT_STATUS = "https://rynadb.herokuapp.com/api/changeprojectstatus";
export const SELECT_PROJECTS = "https://rynadb.herokuapp.com/api/selectproject";

export const ADD_NEW_VIDEO = "https://rynadb.herokuapp.com/api/addnewvideo";
export const CHANGE_VIDEO_STATUS = "https://rynadb.herokuapp.com/api/changevideostatus";
export const DELETE_VIDEO = "https://rynadb.herokuapp.com/api/deletevideo";
export const SELECT_VIDEOS = "https://rynadb.herokuapp.com/api/selectvideos";

export const ADD_NEW_IMAGE = "https://rynadb.herokuapp.com/api/addnewimage";
export const CHANGE_IMAGE_STATUS = "https://rynadb.herokuapp.com/api/changeimagestatus";
export const DELETE_IMAGE = "https://rynadb.herokuapp.com/api/deleteimage";
export const SELECT_IMAGES = "https://rynadb.herokuapp.com/api/selectimages";

export const ADD_NEW_EXPERIENT = "https://rynadb.herokuapp.com/api/addnewexperient";
export const CHANGE_EXPERIENT_STATUS = "https://rynadb.herokuapp.com/api/changeexperientstatus";
export const DELETE_EXPERIENT = "https://rynadb.herokuapp.com/api/deleteexperient";
export const SELECT_EXPERIENTS = "https://rynadb.herokuapp.com/api/selectexperients";

export const ADD_NEW_INFORMATION = "https://rynadb.herokuapp.com/api/addnewinformation";
export const CHANGE_INFORMATION_STATUS = "https://rynadb.herokuapp.com/api/changeinformationtatus";
export const DELETE_INFORMATION = "https://rynadb.herokuapp.com/api/deleteinformation";
export const SELECT_INFORMATIONS = "https://rynadb.herokuapp.com/api/selectinformations";

export const ADD_NEW_EDUCATION = "https://rynadb.herokuapp.com/api/addneweducation";
export const CHANGE_EDUCATION_STATUS = "https://rynadb.herokuapp.com/api/changeeducationstatus";
export const DELETE_EDUCATION = "https://rynadb.herokuapp.com/api/deleteeducation";
export const SELECT_EDUCATIONS = "https://rynadb.herokuapp.com/api/selecteducations";

export const ADD_NEW_SKILL = "https://rynadb.herokuapp.com/api/addnewskill";
export const CHANGE_SKILL_STATUS = "https://rynadb.herokuapp.com/api/changeskillstatus";
export const DELETE_SKILL = "https://rynadb.herokuapp.com/api/deleteskill";
export const SELECT_SKILLS = "https://rynadb.herokuapp.com/api/selectskills";

export const SHOW_PROJECTS = "https://rynadb.herokuapp.com/api/showprojects";
export const SHOW_EXPERIENTS = "https://rynadb.herokuapp.com/api/showexperients";
export const SHOW_INFORMATION = "https://rynadb.herokuapp.com/api/showinformation";
export const SHOW_EDUCATION = "https://rynadb.herokuapp.com/api/showeducation";
export const SHOW_SKILLS = "https://rynadb.herokuapp.com/api/showskills";
export const SELECTED_PROJECT = "https://rynadb.herokuapp.com/api/selectedproject";
export const SELECTED_PROJECT_VIDEOS = "https://rynadb.herokuapp.com/api/selecttedprojectvideos";
export const SELECTED_PROJECT_IMAGES = "https://rynadb.herokuapp.com/api/selectedprojectimages";
export const CHANGE_BACKGROUND = "https://rynadb.herokuapp.com/api/changebackground";
export const CHANGE_CV = "https://rynadb.herokuapp.com/api/changecv";
export const LOGIN = "https://rynadb.herokuapp.com/api/login";
export const CHANGE_TITLE_COLOR = "https://rynadb.herokuapp.com/api/changetitlecolor";
export const CHANGE_LABLE_COLOR = "https://rynadb.herokuapp.com/api/changelablecolor";
export const CHANGE_TEXT_COLOR = "https://rynadb.herokuapp.com/api/changetextcolor";
